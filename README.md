# README #
Our game is PacMan. We will have one character who will have to dodge the ghosts and eat all the orbs in order to finish the game. The player will have three lives. 

Player class
Ghost class
Map class
Wall class
Orb class

Team members:
Taran Dwivedula
- Created basic Player and Orb classes (Control player movement and remove orbs on collision)
- Created Ghost class and automated movement (made it turn when colliding with wall, kills player on collision)
- Added health to player
- Added score variable with getter and setter

Varun Valiveti
-I fixed my Sourcetree issue.
-Scaled the size of the orb so it looks more realistic. 
- Created Ghost class and automated movement (made it turn when colliding with wall, kills player on collision)
- Added health to player
- Added score variable with getter and setter

Yash Aggarwal
-I am attempting to fix my source tree. 

This README would normally document whatever steps are necessary to get your application up and running.


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact