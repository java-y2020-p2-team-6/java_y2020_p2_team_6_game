import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
public class Ghost extends Actor
{
       private int speed = 5;
       private String direction = "right";
       public void act() {
           getImage().scale(40, 40);
           if(direction == "right") {
               setLocation(getX() + speed, getY());
           }
           if(direction == "left") {
               setLocation(getX() - speed, getY());
           }
           if(direction == "down") {
               setLocation(getX(), getY() + speed);
           }
           if(direction == "up") {
               setLocation(getX(), getY() - speed);
           }
        if(getOneIntersectingObject(Wall.class) != null) {
            if(direction == "right") {
                setLocation(getX() - 20, getY());
                int rand = Greenfoot.getRandomNumber(1);
                if(rand == 0) {
                    direction = "up";
                } else {
                    direction = "down";
                }
            } else if(direction == "left") {
                setLocation(getX() + 20, getY());
                int rand = Greenfoot.getRandomNumber(1);
                if(rand == 0) {
                    direction = "up";
                } else {
                    direction = "down";
                }
            } else if(direction == "down") {
                setLocation(getX(), getY() - 20);
                int rand = Greenfoot.getRandomNumber(1);
                if(rand == 0) {
                    direction = "left";
                } else {
                    direction = "right";
                }
            } else {
                setLocation(getX(), getY() + 20);
                int rand = Greenfoot.getRandomNumber(1);
                if(rand == 0) {
                    direction = "left";
                } else {
                    direction = "right";
                }
            }
        }
    }
}
