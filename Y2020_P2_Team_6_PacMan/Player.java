import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
public class Player extends Actor
{
       private int speed = 5;
       private String direction = "right";
       private int health = 1;
       public void act() {
           getImage().scale(40, 40);
           if(direction == "right") {
               setLocation(getX() + speed, getY());
           }
           if(direction == "left") {
               setLocation(getX() - speed, getY());
           }
           if(direction == "down") {
               setLocation(getX(), getY() + speed);
           }
           if(direction == "up") {
               setLocation(getX(), getY() - speed);
           }
           if(Greenfoot.isKeyDown("w")) {
               direction = "up";
           }
           if(Greenfoot.isKeyDown("a")) {
               direction = "left";
           }
           if(Greenfoot.isKeyDown("s")) {
               direction = "down";
           }
           if(Greenfoot.isKeyDown("d")) {
               direction = "right";
           }
           if(getOneIntersectingObject(Ghost.class) != null) {
               health -= 1;
               getWorld().removeObject(this);
               //respawn at spawn point
           }
       }
}
