import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

public class Orb extends Actor
{
    
    public void act() {
        this.getImage().scale(20,20);
        if(getOneIntersectingObject(Player.class) != null) {
            getWorld().setScore(getWorld().getScore() + 1);
            getWorld().removeObject(this);
        }
    }   
}
